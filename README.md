# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Proect Requirements ###
requirements Assignment 6

How	to	create	and	work	with	two-dimensional	(i.e.,	2D)	arrays.
How	to	design	code	that	is	tolerant	of error	cases


Your	task	in	this	assignment	is	to	create	MineSweeper.java. This	will	be	a	somewhat	
simplified	version	of	the	original	“Minesweeper”	game.

Your	version	will	be	simpler	than	these.	In	particular:
 Your	grid	will	be	eight	by	eight	in	size	with	ten	mines.	The	mines	are	to be	
placed	randomly. (Be	sure	to	use	the	Random class	which	is	discussed	on	page	
320 of	the	text.	You	are	not	permitted	to	use Math.random)._

 Your	input	will	be	from	the	computer	keyboard	instead	of	via	the	mouse.	You	
will	output	all	information	to	the	console	window.	If	the	user	enters	an	invalid	
grid	cell	location,	you	must	indicate	the	error	and	ask	until	you	get	a	valid	cell.

The	method	for	clearing	blank grid cells will	be	much	simpler.	If	the	user	
uncovers	a	blank	cell,	you	must	automatically	uncover	only	the	eight	adjacent	
cells (and	your	method	does	not	cascade	any	further	than	this).

 There	will	be	no	score	and	no	timing.	The	user	wins if	they	uncover	all	cells
except	for	those	containing	mines.	The	user	loses if	they	directly	uncover	a	cell
containing	a	mine.
The	input	and	output	of	your	program	must	match	the	sample	shown in	
SampleMineSweeperOutput.pdf.	(Note:	In	this	PDF	the	text	in	red	boxes	is	meant	to	
explain	how	the	program	output	is	related	to	actual	game	play.	You	are	not	meant	to	
produce	the	text	in	these	boxes.)



Bombs should be  number and the rest ust zero
 if the bomb is equal to zero game over 

 Write	the	following	method:
public static ??? initializeFullGrid(???)
where	“???”	corresponds	to	types/parameters you	must	specify. The	method	
will	place	ten	bombs	at	random	in	the	game	grid.	For	each	grid	cell	that	does	
not	contain	a	bomb,	record	the	number	of	bombs	adjacent	to	the	cell.

2. Write	the	following	method:
public static ??? revealGridCell(int row, int col, ???)
where	“???”	corresponds	to	types/parameters you	must	specify.	If	the	cell	is	
blank then	also	uncover	all	neighbouring	cells.	Note:	You	must	be	careful	of	
edge	cases	when	uncovering	neighbours.	If	the	grid	cell	is	at	an	edge,	it	will	
have	fewer	than	eight	neighbours.

3. Write	the	following	method:
public static ??? drawFullGrid(???)
where	“???”	corresponds	to	types/parameters you	must	specify.	The	method	
will	draw	the	current	state	of	the	game	board	to	the	console	window.
Ensure	that	your	code	handles	invalid	row	or	column	indices	throughout	the	
program	so	that	invalid	array	locations	are	never	accessed	(i.e.,	will	not	result	in	the	
program	crashing	with	an	ArrayIndexOutOfBoundsException).

Use	a	2D	array	of	some	type to	keep	track	of	the	game	board.	In	each	cell	you	
could	indicate the	number	of	bombs	neighbouring	the	cell.	You’ll	want	some	
way	to	indicate	that	the	cell	itself	has	a	bomb.
• If	needed,	you	can	use a	second	2D	array	to	keep	track	of	which	grid	cells	have	
or	haven’t	been	uncovered.
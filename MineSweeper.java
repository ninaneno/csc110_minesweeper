import java.util.*;
import java.util.Random;
import java.util.Scanner;

public class MineSweeper {
		//init variables
		public static int minesTotal = 10;
		public static int gridW = 8;
		public static int gridH = 8;


		public static int gameStatus = 0; //0 playing, -1 is a loss, +1 is a win
		public static int lastPlayX =0;
		public static int lastPlayY=0;
		
		// display characteristics
		
		
		public static char floorChar = '.';
		public static char emptyChar = ' ';
		public static char bombChar = 'B';
		public static char deathStepChar = 'X';

		//int [][] column = new int [8][8];
		public static char [][] gridDisplay = new char [gridW][gridH]; // this holds display information, frame buffer
		public static int [][] gridTest = new int [gridW][gridH]; // this holds information as to where the user has checked for a mine
		public static int [][] gridMines = new int [gridW][gridH]; // holds mine placement information, 
		public static int [][] gridMineDistance = new int [gridW][gridH]; // after gridMines are established, 

	public static void main (String[]args){
		//Contructors
		initializeFullGrid();
		renderAll();
		game();


}
public static void game(){
		// main game loop
		while(gameStatus ==0){
			// get user input
			gameInput();
			
			// test against mine grid
			evalPlay();
			// evaluate mine placement
			revealGridCell(lastPlayY,lastPlayX); //row,col = y,x
			drawFullGrid();
			
			

			// render display
		}

		if (gameStatus==-1){ //loss
			// display hidden mine underlay
			
			// loss text
			System.out.print("\n" + " == You loose == ");
			revealGridCell(lastPlayY,lastPlayX); //row,col = y,x
			drawFullGrid();
		}

		if (gameStatus==1){
			revealGridCell(lastPlayY,lastPlayX); //row,col = y,x
			drawFullGrid();
			// win text
			
			
			System.out.print("\n" + " == You Win == ");
		}

	}



public static void initializeFullGrid(){
	generateBlankDisplayGrid();
	generateMineFieldGrid();
	generateMineDistanceGrid();

}	




public static void evalPlay(){
	
	if (mineCheck(lastPlayX,lastPlayY)==1){
		gameStatus=-1;
		//end game
	}
	
	
	
	
}


// console render function
public static void drawFullGrid(){
	//clear console
	System.out.print("\n");
	
	//draw header

		System.out.print("  |");
		for (int x=0; x<gridW;x++){
		System.out.print(" "+x);
		}
	//next
	System.out.print("\n");
		// print horizontal line
		System.out.print("---");
		for (int x=0; x<gridW;x++){
		System.out.print("--");
		}
	System.out.print("\n");
	

		
	//loop over row values
	for (int y=0; y<gridH;y++){
		//row header
		System.out.print(""+ y +" |");
		
		for (int x=0; x<gridW;x++){
			System.out.print(" " + gridDisplay[x][y]);
		}
		System.out.print("\n"); //next row		
	}
	
	
	//draw table
}

public static void gameInput(){
Scanner scanner = new Scanner(System.in);

int xin=-1;
int yin=-1;


boolean playInvalid = true;

Scanner sc = new Scanner(System.in);

while(playInvalid){
do {
    System.out.println("Enter X:");
    while (!sc.hasNextInt()) {
        System.out.println("Invalid-Enter X:");
        sc.next(); // this is important!
    }
    xin = sc.nextInt();
} while (xin <0);


do {
    System.out.println("Enter Y:");
    while (!sc.hasNextInt()) {
        System.out.println("Invalid-Enter Y:");
        sc.next(); // this is important!
    }
    yin = sc.nextInt();
} while (yin < 0);
// test coord
// check if uncoved
//check bomb


//Validate
System.out.print("\n"+ xin +","+ yin);



//TODO: check if already testsed


//continue to game

playInvalid = false; //exit loop

}

//pass
lastPlayX=xin;
lastPlayY=yin;


}




// DEBUG FUNCITONS
public static void renderBasic(int matrix[][]){

		for (int x=0; x<gridW;x++){
			for (int y=0; y<gridH;y++){
				System.out.print(matrix[x][y] + " ");
			}
			System.out.print("\n");
		}

System.out.print("------------------------------\n");
}

public static void renderAll(){
	System.out.print("= Mine Locations =\n");
renderBasic(gridMines);
System.out.print("= Mine distances from cell =\n");
renderBasic(gridMineDistance);

System.out.print("\n=Game=\n\n");
drawFullGrid();

}




// ----------------------------










public static void generateBlankDisplayGrid(){
	// itterate across space
	for (int x=0; x<gridW;x++){
		for (int y=0; y<gridH;y++){
			gridDisplay[x][y] = floorChar; 
			
		}
	}
}



public static void generateMineFieldGrid(){
			//int minesTotal = 10;int gridW = 8;int gridH = 8;
			Random r = new Random();
			int mineCount = 0;
		
			while(mineCount < minesTotal){ //Place mines
				int x = r.nextInt(gridW); //get random mine x pos.
				int y = r.nextInt(gridH); //get random mine y pos.
				if (gridMines[x][y] != 1){ // check if x,y mine pos. is already taken; if not
					gridMines[x][y] = 1; // set location equal to 1 for each mine
					mineCount++;
				}
			}
}


public static void generateMineDistanceGrid(){
	// itterate across vminefield
	for (int qx=0; qx<gridW;qx++){
		for (int qy=0; qy<gridH;qy++){
			gridMineDistance[qx][qy] = minesAdjacent(qx,qy); // apply search across grid
		}
	}
}






  public static int mineCheck(int y, int x) { // basic check for distance grid
    if((y >= 0 && x >= 0) && (x < gridW && y < gridH)){ // check for errors
      if (gridMines[y][x] == 1) { // check for mine at grid location
      return 1;
    } 
   }else {
      return 0;
    
}
return 0;
}




public static void revealGridCell(int row, int col){
	//gridDisplay
		//gridTest
		//gridMines
		//gridMineDistance
		
		//bombChar = 'B';
		//floorChar = '.';
		//emptyChar = ' ';

	
	// call frame update
	
	
	// ===================================== Play Grid
	if ( gameStatus ==0 ){
			for (int x=0; x<gridW;x++){
			for (int y=0; y<gridH;y++){
				if (row==y && col==x) {//user pos
					if(gridMineDistance[x][y] >0){ //show hint
						gridDisplay[x][y] = Integer.toString(gridMineDistance[x][y]).charAt(0); // convert integer estimate value to char
					}
					if(gridMineDistance[x][y] ==0){
						gridDisplay[x][y] = emptyChar;
						// TODO: if more empty beside, reveal
					}
				}
			}}
		
	}
	
	// ===================================== Win Grid
	if(gameStatus==1){
			for (int x=0; x<gridW;x++){
			for (int y=0; y<gridH;y++){
				if(gridMineDistance[x][y] >0){ //show all hints
						gridDisplay[x][y] = Integer.toString(gridMineDistance[x][y]).charAt(0); // convert integer estimate value to char
					}else if(gridMines[x][y] == 1){ // draw mine
						gridDisplay[x][y] = bombChar;
					}else{
						gridDisplay[x][y] = emptyChar;
					}
				
			}}
		
	}
	
	// ===================================== Loss Grid
	if(gameStatus==-1){
		System.out.println("====loss rendertest==");
			for (int x=0; x<gridW;x++){
			for (int y=0; y<gridH;y++){
				if (row==y && col==x) {//user pos, to display what was the loosing position played
					gridDisplay[x][y] = deathStepChar;
				}else{
					if(gridMineDistance[x][y] >0){ //show all hints
						gridDisplay[x][y] = Integer.toString(gridMineDistance[x][y]).charAt(0); // convert integer estimate value to char
					}else if(gridMines[x][y] == 1){ // draw mine
						gridDisplay[x][y] = bombChar;
					}else{
						gridDisplay[x][y] = emptyChar;
					
					
				}
				
					
				
			}}
	}	
		
		
		
	}
}









public static int minesAdjacent(int y, int x) {
    int minesCountedNear = 0;
    // check mines in all directions
    minesCountedNear += mineCheck(y - 1, x - 1);  // NW
    minesCountedNear += mineCheck(y - 1, x);      // N
    minesCountedNear += mineCheck(y - 1, x + 1);  // NE
    minesCountedNear += mineCheck(y, x - 1);      // W
    minesCountedNear += mineCheck(y, x + 1);      // E
    minesCountedNear += mineCheck(y + 1, x - 1);  // SW
    minesCountedNear += mineCheck(y + 1, x);      // S
    minesCountedNear += mineCheck(y + 1, x + 1);  // SE

    return minesCountedNear;
 }
 
 
 
 
 
 
}
